
const FIRST_NAME = "Maria";
const LAST_NAME = "Stoian";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache={
        about: 0,
        home: 0,
        get contact(){return this.home;},
        pageAccessCounter: function(x)
        {
            let that=this;
            if(typeof (x) !=='undefined')
            {
                if('about'===x.toLowerCase())
                    that.about=that.about+1;
                if(('contact'===x.toLowerCase()))
                    that.home=that.home+1;
            }
            else
                that.home=that.home+1;
            this.about=that.about;
            this.home=that.home; 
        },
        getCache: function()
        {
            //return Object.values(cache);
            return this;
        }
    };
    return cache;
}
module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

